#ifndef _VL_API_H
#define _VL_API_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

/* defined in db.c */
int *db_getservers(int all);
int db_getserveraddr(int id, char* addr, char* port);
int db_updateserver(int id, char* name, char* mode, char* motd, int players_cnt, int public, char* version, char* features);
int db_updateplayer(int id, char* name, int server);

/* defined in http.c */
int http_get(char* host, char* port, char* url, char* buff, int size);

/* defined in lib.c */
int valid_mode(char*);
int valid_version(char*);

#endif
