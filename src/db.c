/************************************************************************
* db.c
* voxelands - api backend status daemon
* Copyright (C) Lisa Milne 2015 <lisa@ltmnet.com>
*
* This file is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include <my_global.h>
#include <mysql.h>

#include "vl-api.h"
#include "config.h"

#include <string.h>
#include <time.h>

MYSQL *dbconn = NULL;
/*

CREATE DATABASE voxelands ;

USE voxelands ;

CREATE TABLE `servers` (
 `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
 `name` VARCHAR (255) NOT NULL,
 `addr` VARCHAR (255) NOT NULL,
 `port` INT(11) UNSIGNED,
 `mode` VARCHAR (20) NOT NULL,
 `motd` VARCHAR (255) NOT NULL,
 `players` INT(11) UNSIGNED NOT NULL DEFAULT '0',
 `public` VARCHAR (10) NOT NULL,
 `version` VARCHAR (50) NOT NULL,
 `features` VARCHAR (255) NOT NULL,
 `lastreply` INT(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin ;

CREATE TABLE `players` (
 `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
 `name` VARCHAR(255) NOT NULL,
 `hash` VARCHAR(255) NOT NULL,
 `cookie` VARCHAR(255) NOT NULL,
 `server` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin ;

CREATE USER 'apiuser'@'localhost' IDENTIFIED BY 'apipass';
GRANT SELECT,INSERT,UPDATE ON `servers` TO 'apiuser'@'localhost';
GRANT SELECT,INSERT,UPDATE ON `players` TO 'apiuser'@'localhost';

 */

int db_connect()
{
	MYSQL *c;
	if ((dbconn = mysql_init(NULL)) == NULL)
		return -1;

	c = mysql_real_connect(dbconn, DB_HOST, DB_USER, DB_PASS, DB_NAME, 0, NULL, 0);
	if (!c)
		return -1;

	return 0;
}

int db_close()
{
	if (dbconn)
		mysql_close(dbconn);
	dbconn = NULL;
	return 0;
}

int *db_getservers(int all)
{
	char q[1024];
	int *r;
	MYSQL_RES *result;
	MYSQL_ROW row;
	int rows;
	int i;

	if (all) {
		sprintf(q,"SELECT `id` FROM `servers`");
	}else{
		int lr = time(NULL);
		lr -= 600; /* 10 minutes */
		sprintf(q,"SELECT `id` FROM `servers` WHERE `lastreply` > %d",lr);
	}

	if (db_connect())
		return NULL;

	mysql_query(dbconn,q);

	result = mysql_store_result(dbconn);
	if (!result) {
		db_close();
		return NULL;
	}

	rows = mysql_num_rows(result);
	if (!rows) {
		mysql_free_result(result);
		db_close();
		return NULL;
	}

	r = malloc(sizeof(int)*(rows+1));
	if (!r) {
		mysql_free_result(result);
		db_close();
		return NULL;
	}

	for (i=0; (row = mysql_fetch_row(result)); i++) {
		unsigned long *lengths;
		lengths = mysql_fetch_lengths(result);
		if (strncpy(q,row[0],lengths[0])) {
			r[i] = strtol(q,NULL,10);
		}else{
			r[i] = 0;
			break;
		}
	}

	r[i] = 0;

	mysql_free_result(result);

	db_close();

	return r;
}

int db_getserveraddr(int id, char* addr, char* port)
{
	char q[1024];
	MYSQL_RES *result;
	MYSQL_ROW row;
	unsigned long *lengths;

	if (!id || !addr || !port)
		return -1;

	sprintf(q,"SELECT `addr` , `port` FROM `servers` WHERE `id` = %d",id);

	if (db_connect())
		return -1;

	mysql_query(dbconn,q);

	result = mysql_store_result(dbconn);
	if (!result) {
		db_close();
		return -1;
	}

	if (!mysql_num_rows(result)) {
		mysql_free_result(result);
		db_close();
		return -1;
	}

	if (!(row = mysql_fetch_row(result))) {
		mysql_free_result(result);
		db_close();
		return -1;
	}

	lengths = mysql_fetch_lengths(result);
	if (!memcpy(addr,row[0],lengths[0])) {
		mysql_free_result(result);
		db_close();
		return -1;
	}
	addr[lengths[0]] = 0;
	if (!memcpy(port,row[1],lengths[1])) {
		mysql_free_result(result);
		db_close();
		return -1;
	}
	port[lengths[1]] = 0;

	mysql_free_result(result);

	db_close();

	return 0;
}

int db_updateserver(int id, char* name, char* mode, char* motd, int players_cnt, int public, char* version, char* features)
{
	int l;
	char q[4096];
	char e_name[512];
	char e_mode[512];
	char e_motd[512];
	char e_version[512];
	char e_features[512];

	if (id < 1)
		return -1;

	if (db_connect())
		return -1;

	l = strlen(name);
	if (l > 255) {
		db_close();
		return -1;
	}
	mysql_real_escape_string(dbconn,e_name,name,l);

	if (!valid_mode(mode))
		mode = "adventure";
	l = strlen(mode);
	if (l > 255) {
		db_close();
		return -1;
	}
	mysql_real_escape_string(dbconn,e_mode,mode,l);

	if (!motd)
		motd = "";
	l = strlen(motd);
	if (l > 255) {
		db_close();
		return -1;
	}
	mysql_real_escape_string(dbconn,e_motd,motd,l);

	if (!valid_version(version))
		version = "???";
	l = strlen(version);
	if (l > 255) {
		db_close();
		return -1;
	}
	mysql_real_escape_string(dbconn,e_version,version,l);

	if (!features)
		features = "";
	l = strlen(features);
	if (l > 255) {
		db_close();
		return -1;
	}
	mysql_real_escape_string(dbconn,e_features,features,l);

	sprintf(
		q,
		"UPDATE `servers` "
		"SET `name` = '%s' , `mode` = '%s' , `motd` = '%s' , `players` = %d , `public` = %d , `version` = '%s' , `features` = '%s' , `lastreply` = %d "
		"WHERE `id` = %d",
		e_name,
		e_mode,
		e_motd,
		players_cnt,
		public,
		e_version,
		e_features,
		(int)time(NULL),
		id
	);

	mysql_query(dbconn,q);

	l = mysql_affected_rows(dbconn);

	db_close();

	return l;
}

int db_updateplayer(int id, char* name, int server)
{
	return -1;
}
