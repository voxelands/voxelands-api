/************************************************************************
* main.c
* voxelands - api backend status daemon
* Copyright (C) Lisa Milne 2015 <lisa@ltmnet.com>
*
* This file is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "vl-api.h"
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>

static void check_server(int id)
{
	char addr[512];
	char port[512];
	char buff[1024];
	char players[1024];
	char* version;
	char* name;
	char* motd;
	char* adr;
	char* prt;
	char* mode;
	char* public;
	int pub;
	char* privs;
	char* features;
	int players_cnt;

	if (db_getserveraddr(id,addr,port))
		return;
	if (http_get(addr,port,"/api",buff,1024))
		return;
	if (http_get(addr,port,"/api/players",players,1024))
		return;
	players_cnt = strtol(players,NULL,10);
	version = buff;
	name = strchr(version,'\n');
	if (!name)
		return;
	*name = 0;
	name++;
	motd = strchr(name,'\n');
	if (!motd)
		return;
	*motd = 0;
	motd++;
	adr = strchr(motd,'\n');
	if (!adr)
		return;
	*adr = 0;
	adr++;
	prt = strchr(adr,'\n');
	if (!prt)
		return;
	*prt = 0;
	prt++;
	mode = strchr(prt,'\n');
	if (!mode)
		return;
	*mode = 0;
	mode++;
	public = strchr(mode,'\n');
	if (!public)
		return;
	*public = 0;
	public++;
	pub = !strcmp(public,"public");
	privs = strchr(public,'\n');
	if (!privs)
		return;
	*privs = 0;
	privs++;
	features = strchr(privs,'\n');
	if (!features)
		return;
	db_updateserver(id,name,mode,motd,players_cnt,pub,version,features);
}

/* print help and usage */
static void usage()
{
	printf(
		"%s %s - voxelands api backend status daemon\n\n"
		"USAGE: %s [OPTIONS]\n\n"
		"OPTIONS:\n"
		"  -d Daemonise the process (default).\n"
		"  -D Do not daemonise the process.\n"
		"  -a Check all servers on startup (default).\n"
		"  -A Do not check all servers on startup.\n"
		"  -o Run check only once.\n"
		"  -O Run check repeatedly in a loop (default).\n"
		"  -? Show this help info.\n",
		TARGET,
		VERSION,
		TARGET
	);
}

int main(int argc, char** argv)
{
	int i;
	int k;
	int *s;
	time_t b;
	time_t e;
	int daemonise = 1;
	int all = 1;
	int once = 0;

	for (i=1; i<argc; i++) {
		if (argv[i][0] == '-') {
			for (k=1; argv[i][k]; k++) {
				switch (argv[i][k]) {
				case 'd':
					daemonise = 1;
					break;
				case 'D':
					daemonise = 0;
					break;
				case 'a':
					all = 1;
					break;
				case 'A':
					all = 0;
					break;
				case 'o':
					once = 1;
					break;
				case 'O':
					once = 0;
					break;
				case '?':
					usage();
					return 0;
					break;
				case '-':
					if (k == 1 && argv[i][2]) {
						k = -2;
					}else{
						k = -1;
					}
					break;
				default:
					fprintf(stderr,"%s: invalid option '%c'\n",TARGET,argv[i][k]);
					usage();
					return 1;
				}
			}
		}else{
			usage();
			return 1;
		}
	}

	/* daemonise */
	if (daemonise) {
		signal(SIGCHLD,SIG_IGN);
		signal(SIGHUP, SIG_IGN);
		signal(SIGPIPE,SIG_IGN);

		for (i=0; i<32; i++) {
			close(i);
		}

		i = fork();
		if (i < 0)
			return 1;

		if (i)
			return 0;

		setpgrp();
	}

	/* first check all */
	s = db_getservers(all);
	if (s) {
		for (i=0; s[i]!=0; i++) {
			check_server(s[i]);
		}
		free(s);
	}

	if (once)
		return 0;

	/* then loop checking recent */
	while (1) {
		b = time(NULL);
		s = db_getservers(0);
		if (s) {
			for (i=0; s[i]!=0; i++) {
				check_server(s[i]);
			}
			free(s);
		}
		e = time(NULL);

		if (e <= b) {
			sleep(300);
		}else if (e-b > 250) {
			sleep(1);
		}else{
			sleep(300-(e-b));
		}
	}

	return 0;
}
