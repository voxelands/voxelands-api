/************************************************************************
* http.c
* voxelands - api backend status daemon
* Copyright (C) Lisa Milne 2015 <lisa@ltmnet.com>
*
* This file is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "vl-api.h"
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <errno.h>

int http_get(char* host, char* port, char* url, char* buff, int size)
{
	struct addrinfo hints;
	struct addrinfo *addr;
	int fd;

	fd_set rfds;
	struct timeval tv;
	int s;

	char* e;
	int length;
	int tlength;
	char request[2048];
	char in[2048];

	if (!strcmp(host,"0.0.0.0"))
		return -1;

	length = sprintf(
		request,
		"GET %s HTTP/1.1\r\n"
		"Host: %s\r\n"
		"From: darkrose\r\n"
		"User-Agent: Voxelands-API\r\n"
		"Connection: close\r\n\r\n",
		url,
		host
	);

	if (!length)
		return -2;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	/* resolve hostname */
	if (getaddrinfo(host, port, &hints, &addr))
		return -3;

	/* open socket */
	if ((fd = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol)) == -1) {
		freeaddrinfo(addr);
		return -4;
	}

	fcntl(fd, F_SETFL, O_NONBLOCK);

	/* connect to server */
	s = connect(fd, addr->ai_addr, addr->ai_addrlen);
	if (s != 0) {
		struct epoll_event event;
		int efd = -1;
		struct epoll_event events;
		unsigned int count = -1;
		int r = -1;
		socklen_t rlen = sizeof(r);

		if (errno != EINPROGRESS) {
			freeaddrinfo(addr);
			shutdown(fd,2);
			return -5;
		}

		if ((efd = epoll_create(1)) == -1) {
			freeaddrinfo(addr);
			shutdown(fd,2);
			return -5;
		}

		event.data.fd = fd;
		event.events = EPOLLOUT | EPOLLIN | EPOLLERR;

		if (epoll_ctl(efd, EPOLL_CTL_ADD, fd, &event) == -1) {
			freeaddrinfo(addr);
			shutdown(fd,2);
			return -5;
		}

		count = epoll_wait(efd, &events, 1, 1000);

		if (count < 0) {
			freeaddrinfo(addr);
			shutdown(fd,2);
			return -5;
		}

		if (getsockopt(fd, SOL_SOCKET, SO_ERROR, &r, &rlen) < 0) {
			freeaddrinfo(addr);
			shutdown(fd,2);
			return -5;
		}

		if (r != 0) {
			freeaddrinfo(addr);
			shutdown(fd,2);
			return -5;
		}
	}

	/* get the page */
	write(fd,request,length);

	tlength = 0;

	tv.tv_sec = 0;
	tv.tv_usec = 1000000;
	FD_ZERO(&rfds);
	FD_SET(fd, &rfds);
	if (select(fd+1, &rfds, NULL, NULL, &tv) < 1) {
		freeaddrinfo(addr);
		shutdown(fd,2);
		return -6;
	}

	if (!FD_ISSET(fd,&rfds)) {
		freeaddrinfo(addr);
		shutdown(fd,2);
		return -7;
	}

	while (tlength < 2046) {
		tv.tv_sec = 0;
		tv.tv_usec = 100000;
		FD_ZERO(&rfds);
		FD_SET(fd, &rfds);
		if (select(fd+1, &rfds, NULL, NULL, &tv) < 1 || !FD_ISSET(fd,&rfds))
			break;
		length = read(fd,in,2047);
		if (length < 1)
			break;
		if (tlength+length > 2047)
			length = 2046-tlength;
		memcpy(request+tlength,in,length);
		tlength += length;
	}
	shutdown(fd,2);
	freeaddrinfo(addr);

	if (tlength < 1)
		return -8;

	request[tlength] = 0;

	e = strstr(request,"\r\n\r\n");
	if (!e)
		return -9;

	if (!strncpy(buff,e+4,size))
		return -10;

	return 0;
}
