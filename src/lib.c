/************************************************************************
* lib.c
* voxelands - api backend status daemon
* Copyright (C) Lisa Milne 2015 <lisa@ltmnet.com>
*
* This file is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "vl-api.h"
#include <string.h>

int valid_mode(char* txt)
{
	if (!txt || !txt[0])
		return 0;
	if (!strcmp(txt,"creative"))
		return 1;
	if (!strcmp(txt,"survival"))
		return 1;
	if (!strcmp(txt,"adventure"))
		return 1;
	if (!strcmp(txt,"custom"))
		return 1;
	return 0;
}

int valid_version(char* txt)
{
	char* dot;
	char* colon;

	if (!txt || !txt[0])
		return 0;
	dot = strchr(txt,'.');
	if (!dot)
		return 0;
	if (dot-txt != 4)
		return 0;
	colon = strchr(dot,':');
	if (colon) {
		if (colon-dot != 3)
			return 0;
	}else if (strlen(dot) != 3) {
		return 0;
	}
	return 1;
}
