CC ?= gcc

PREFIX ?= /usr
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man/man1

SRCDIR=src
INCDIR=inc

TARGET=vl-checker
VERSION=0.1

TARGET_CFLAGS ?= $(shell mysql_config --cflags) -Wall -g -DTARGET=\"$(TARGET)\" -DVERSION=\"$(VERSION)\" -Iinc/ -I. $(CFLAGS)
TARGET_CPPFLAGS ?= $(CPPFLAGS)
TARGET_CLIBS ?= $(shell mysql_config --libs) $(CLIBS)
TARGET_LDFLAGS ?= $(LDFLAGS)

OBJS=$(SRCDIR)/main.o $(SRCDIR)/db.o $(SRCDIR)/lib.o $(SRCDIR)/http.o
DISTFILES=man $(SRCDIR) $(INCDIR) config.h Makefile* CHANGELOG LICENSE README

all: default

default: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(TARGET_LDFLAGS) -o $(TARGET) $(OBJS) $(TARGET_CLIBS)

dist-base:
	mkdir -p $(TARGET)-$(VERSION)
	cp -Rt $(TARGET)-$(VERSION) $(DISTFILES)

dist-gz: dist-base
	tar czf $(TARGET)-$(VERSION).tar.gz $(TARGET)-$(VERSION)
	rm -r $(TARGET)-$(VERSION)

dist-bz2: dist-base
	tar cjf $(TARGET)-$(VERSION).tar.bz2 $(TARGET)-$(VERSION)
	rm -r $(TARGET)-$(VERSION)

dist: dist-bz2

distclean:
	rm -f $(OBJS)

clean: distclean
	rm -f $(TARGET)*

install: $(TARGET)
	install $(TARGET) $(BINDIR)/$(TARGET)
	-install man/$(TARGET).1 $(MANDIR)/$(TARGET).1

uninstall:
	rm -f $(BINDIR)/$(TARGET)
	rm -f $(MANDIR)/$(TARGET).1

fresh: clean all

$(SRCDIR)/%.o: $(SRCDIR)/%.c inc/vl-api.h config.h
	$(CC) $(TARGET_CPPFLAGS) $(TARGET_CFLAGS) -o $@ -c $<

.PHONY: all default distclean dist dist-base dist-bz2 dist-gz clean fresh install uninstall
